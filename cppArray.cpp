/*
    *Program for simple array operations
*/

#include<iostream>
using namespace std;
class cppArray{
	public:
		int a[20],b[20],n,i,j;
		int getSize(int);
		void accept();
		void display();
		void getRange();
		void exchange();
		void sort();
};

int cppArray::getSize(int a){
	n = a;
	return n;
}

void cppArray::accept(){
	cout<<"Enter first array: \n";
	for(i=0;  i<n;  i++){
		cin>>a[i];
	}
	
	cout<<"Enter second array: \n";
	for(j=0; j<n; j++){
		cin>>b[j];
	}
}

void cppArray::display(){
	cout<<"First array: \n";
	for(i=0; i<n; i++){
		cout<<a[i]<<"\t";
	}
	
	cout<<"\nSecond array: \n";
	for(i=0; i<n; i++){
		cout<<b[i]<<"\t";
	}
}

void cppArray::getRange(){

	cout<<"\n\nThe range of first array is from 0 to "<<n-1;
	cout<<"\nThe range of first array is from 0 to "<<n-1;
}

void cppArray::exchange(){
	
	int temp;
	for(i=0; i<n; i++){
		temp = a[i];
		a[i] = b[i];
		b[i] = temp;
	}

}

void cppArray::sort(){
	int temp;
	for(i=0; i<n; i++){
		for(j=0; j<(n-1); j++){
			if(a[j] > a[j+1]){
				temp = a[j];
				a[j] = a[j+1];
				a[j+1] = temp;
			}
		}
	}
	
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			if(b[j] > b[j+1]){
				temp = b[j];
				b[j] = b[j+1];
				b[j+1] = temp;
			}
		}
	}

}

int main(){
	cppArray cA;
	int size;
	cout<<"Enter array size:";
	cin>>size;
	cA.getSize(size);
	cA.accept();
	cA.sort();
	cout<<"\n\nSorted arrays:\n";
	cA.display();
	cA.exchange();
	cout<<"\n\nExchanged arrays: \n";
	cA.display();
	cA.getRange();
	cout<<"\n";
	return 0;
}
