/*
    * Simple example of exception handling.
    * Multiple catch() blocks can be used.
*/
#include<iostream>
using namespace std;

class Person{
	public:
		int age,wheeler;
		char name[30];
		long int salary;
		void accept();
		void display();
};

void Person::accept(){

	cout<<"Enter your name:\n";
	cin>>name;
	try{
		cout<<"Enter your age:\n";
		cin>>age;
		if(age < 18 || age > 55){
			throw 10;
		}
		
		cout<<"Vehicle type? (2/4):\n";
		cin>>wheeler;
		if(wheeler != 4){
			throw 100;
		}
		
		cout<<"Enter your income:\n";
		cin>>salary;
		if(salary < 50000 || salary > 100000){
			throw 1000;
		}
		
	}catch(...){
		cout<<"Exception caught!\nProgam terminated.\n";
	}	
}

void Person::display(){
	
	cout<<"Name: "<<name<<endl;
	cout<<"Age: "<<age<<endl;
	cout<<"Vehicle type: "<<wheeler<<" wheeler."<<endl;
	cout<<"Income: "<<salary<<" /-"<<endl;
}

int main(){

	Person pers;
	int choice,i,j;
	do{
		cout<<"1.Enter data\n2.Display\nEnter your choice:\n";
		cin>>choice;
		switch(choice){
			case 1:
				pers.accept();
				break;
			case 2:
				pers.display();
				break;
			default:
				cout<<"Wrong choice!\n";
				break;
		}
		i++;
		cout<<"Want to continue? (1/0)\n";
		cin>>j;
	}while(j == 1);
	return 0;
}
