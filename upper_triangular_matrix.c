/*
	* Upper triangular matrix.
*/
#include<stdio.h>
int main(){
	int n,m,i,j;

	printf("Enter no of rows: \n");
	scanf("%d", &n);
	printf("Enter no of columns: \n");
	scanf("%d", &m);
	
	int a[n][m];
	printf("\nEnter a %d x %d matrix: \n",n,m);
	for(i=0; i<n; i++){
		for(j=0; j<m; j++){
			scanf("%d", &a[i][j]);
		}
	}

	printf("The upper triangular matrix is: \n");
	for(i=0; i<n; i++){
		for(j=0; j<m; j++){
			if(i > j){
				a[i][j] = 0;
			}
		}
	}
	for(i=0; i<n; i++){
		printf("|\t");
		for(j=0; j<m; j++){
			printf("%d\t", a[i][j]);
		}
		printf("|\n");
	}
	return 0;
}
