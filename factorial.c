/*
	* Factorial of a given number.
*/

#include<stdio.h>
int main(){
	int n,i,fact = 0;
	
	printf("Enter a number: \n");
	scanf("%d", &n);

	for(i=n; i>0; i--){
		fact += n * i;
	}

	printf("Factoroal of %d is: %d\n",n,fact);
	return 0;
}
