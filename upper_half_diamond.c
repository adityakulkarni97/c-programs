/*
	* Upper half diamond.
*/
#include<stdio.h>
int main(){

	int i,j,n,mid=1;
	
	printf("Enter no of lines: \n");
	scanf("%d",&n);

	for(i=0; i<n; i++){
		for(j=i;j<n;j++){
			printf("");
		}
		printf("*");
		if(i != 0){

			for(j=0;j<mid;j++){
				printf(" ");
			}
			mid += 2;
			printf("*");
		}
		printf("\n");
	}
	return 0;
}
