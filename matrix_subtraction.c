/*
	* Matrix subtraction.
*/
#include<stdio.h>
int main(){
	int i,j,m,n;
	
	printf("Enter no of rows: \n");
	scanf("%d",&n);
	printf("\nEnter no of columns: \n");
	scanf("%d",&m);
	int a[n][m],b[n][m],c[n][m];

	printf("\nEnter first %d x %d matrix: \n",n,m);
	for(i=0; i<n; i++){
	
		for(j=0;j<m;j++){
			scanf("%d", &a[i][j]);
		}
	}

	printf("\nEnter second %d x %d matrix: \n",n,m);
	for(i=0; i<n; i++){
	
		for(j=0;j<m;j++){
			scanf("%d", &b[i][j]);
		}
	}

	for(i=0; i<n; i++){
	
		for(j=0;j<m;j++){
			c[i][j] = a[i][j] - b[i][j];
		}
	}

	printf("\nEnter resultant matrix is: \n",n,m);
	for(i=0; i<n; i++){
		printf("|\t");	
		for(j=0;j<m;j++){
			printf("%d\t", c[i][j]);
		}
		printf("|\n");
	}
	
	printf("\n");
	return 0;
}
